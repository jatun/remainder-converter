/*
 *
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.jatun.training.test.rc;

import com.jatun.training.rc.Converter;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Ivan Alban
 */
public class ConverterTest extends AbstractTest {

    public static final int VALUE = 1147;

    @Test
    public void binaryTest() {
        String result = Converter.getInstance().toBinary(VALUE);

        Assert.assertEquals(result, "10001111011");
    }

    @Test
    public void octalTest() {
        String result = Converter.getInstance().toOctal(VALUE);

        Assert.assertEquals(result, "2173");
    }

    @Test
    public void hexadecimalTest() {
        String result = Converter.getInstance().toHexadecimal(VALUE);

        Assert.assertEquals(result, "47B");
    }
}
