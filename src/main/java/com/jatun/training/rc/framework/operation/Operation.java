/*
 *
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.jatun.training.rc.framework.operation;

import com.jatun.training.rc.framework.visitor.Visitable;
import com.jatun.training.rc.framework.visitor.Visitor;
import com.jatun.training.rc.model.Element;

/**
 * @author Ivan Alban
 */
public abstract class Operation implements Visitor {

    @Override
    public void visit(Visitable visitable) {
        Element element = (Element) visitable;

        Integer remainder = element.getSource() % this.base();
        Integer result = element.getSource() / this.base();

        element.setRemainder(remainder);
        element.setResult(result);
    }

    protected abstract Integer base();
}
