/*
 *
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.jatun.training.rc.framework.operation;

import com.jatun.training.rc.model.Element;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Ivan Alban
 */
public abstract class Algorithm<T extends Operation> {

    public final List<Element> apply(Integer source) {
        T op = this.operation();

        Element first = new Element(source);
        first.accept(op);

        LinkedList<Element> elements = new LinkedList<>();
        elements.add(first);

        while (elements.getLast().getResult() > 0) {
            Integer next = elements.getLast().getResult();

            Element nextElement = new Element(next);
            nextElement.accept(op);

            elements.add(nextElement);
        }

        Collections.reverse(elements);

        return elements;
    }

    protected abstract T operation();
}
