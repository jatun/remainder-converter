/*
 *
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.jatun.training.rc;

import com.jatun.training.rc.binary.BinaryAlgorithm;
import com.jatun.training.rc.binary.BinarySerializer;
import com.jatun.training.rc.framework.operation.Algorithm;
import com.jatun.training.rc.framework.operation.Serializer;
import com.jatun.training.rc.hexadecimal.HexadecimalAlgorithm;
import com.jatun.training.rc.hexadecimal.HexadecimalSerializer;
import com.jatun.training.rc.octal.OctalAlgorithm;
import com.jatun.training.rc.octal.OctalSerializer;

/**
 * @author Ivan Alban
 */
public class Converter {

    public static Converter getInstance() {
        return new Converter();
    }

    private Converter() {
    }

    public String toBinary(Integer value) {
        return this.convert(value, new BinaryAlgorithm(), new BinarySerializer());
    }

    public String toOctal(Integer value) {
        return this.convert(value, new OctalAlgorithm(), new OctalSerializer());
    }

    public String toHexadecimal(Integer value) {
        return this.convert(value, new HexadecimalAlgorithm(), new HexadecimalSerializer());
    }

    private String convert(Integer value, Algorithm<?> algorithm, Serializer serializer) {
        return serializer.serialize(algorithm.apply(value));
    }
}
