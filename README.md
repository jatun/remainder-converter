# Remainder Converter

## Remainder Algorithm  

The remainder algorithm is a common method to convert a number from decimal base to a different base.

## Visitor Pattern

You can found the basic information about this pattern in [Wikipedia Visitor Pattern](https://en.wikipedia.org/wiki/Visitor_pattern).

This implementation intends to expose a use case for the visitor pattern.

## Packages

#### Framework Visitor

Contains the visitor pattern components.

#### Framework Operation

Contains the base algorithm implementation, the required operations, and a serializer to build the new number as a string.

#### Model

Contains the core structure on which the algorithm does its operations.
 
#### Binary

Contains the implementation to change a number from decimal base to binary base.

#### Octal

Contains the implementation to change a number from decimal base to octal base.